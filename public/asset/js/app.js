$(function () {
    var quizContainer = $('#quiz-container'),
        quizesListSelect = $('#quiz')
    ;

    // Preload quizes
    $.ajax({
        url: '/initialize',
        method: 'get',
        success: function (data) {
            if (0 == data.step) {
                $.each(data.questions, function (_, test) {
                    quizesListSelect.append('<option value="' + test.id + '">' + test.name + '</option>');
                });
                return;
            }
            progress.setCurrent(data.currentProgress);
            progress.setMax(data.questionsCount);

            loadQuestion(data.quiz, data.question);
        }
    });

    //region Handle authentication
    $('body').on('submit', '#authenticate', function (event) {
        event.preventDefault();
        var name = $('#name').val().trim();
        var quiz = parseInt($('#quiz').val());
        var url = $(this).attr('action');

        if (!name || !quiz) {
            alert('Both name and quiz are required');
            return;
        }

        $.ajax({
            method: 'post',
            url: url,
            data: {
                name: name,
                quiz: quiz
            },
            success: function (data) {
                if (data.code) {
                    if (1 == data.code) {
                        quizContainer.html(
                            '<div class="alert alert-success">'
                            + 'Hi, ' + data.name + '! We are loading your first question!'
                            + '</div>'
                        );
                    }
                    progress.setCurrent(0);
                    progress.setMax(data.questionsCount);
                    loadQuestion(data.quiz);

                    return;
                }

                alert(data.message);
            },
            error: function (e) {
                alert('An error occurred!');
            }
        });
    });
    //endregion

    //region Load question
    var loadQuestion = function (quizId, questionId) {
        progress.show();
        $.ajax({
            url: '/question',
            method: 'get',
            data: {
                question_id: questionId,
                quiz_id: quizId
            },
            success: function (data) {
                if (1 == data.code) {
                    progress.setCurrent(data.currentProgress);
                    placeQuestion(data.question);
                    return;
                }

                if (-1 == data.code) {
                    alert('This quiz has no questions! Choose another.');
                    location.reload();
                    return;
                }

                alert(data.message);
            },
            error: function (e) {
                alert('An error occurred!');
            }
        });
    };
    //endregion

    var placeQuestion = function (question) {
        var html = $('<div></div>');
        $('<h3>' + question.question + '</h3>').appendTo(html);

        var form = $('<form></form>')
            .attr('id', 'question-form')
            .attr('action', '/answer')
            .attr('method', 'post')
        ;

        $('<input type="hidden" id="question_id" value="' + question.id + '" />').appendTo(form);
        var row = $('<div class="row"></div>');

        $.each(question.answers, function (id, answer) {
            var radioHtml = '\n' +
                '<div class="col-12 col-md-6 padded">' +
                '   <div class="btn-group-toggle" data-toggle="buttons">' +
                '       <label class="btn btn-info btn-block is-answer">\n' +
                '           <input type="radio" value="' + answer.id + '" name="question-' + question.id + '" id="answer-' + answer.id + '" autocomplete="off"> ' + answer.answer +
                '       </label>' +
                '   </div>' +
                '</div>'
            ;

            $(radioHtml).appendTo(row);
        });

        row.appendTo(form);

        $('<button type="submit" class="btn btn-primary">Next question</button>').appendTo(form);

        form.appendTo(html);
        quizContainer.html(html);
    };

    $('body').on('click', '.is-answer', function () {
        $('.is-answer').removeClass('active');
        $(this).button('toggle');
    });

    //region Handle next question click
    $('body').on('submit', '#question-form', function (e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var questionId = $('#question_id').val();
        var answer = $('input[name="question-' + questionId + '"]:checked').val();

        if (!answer) {
            alert('You need to pick an answer');
            return;
        }

        if (!questionId || !url) {
            alert('Something went wrong, please refresh.');
            return;
        }

        $.ajax({
            url: url,
            method: 'post',
            data: {
                question_id: questionId,
                answer_id: answer
            },
            success: function (data) {
                if (data.code) {
                    if (data.question) {
                        // We have next question
                        progress.setCurrent(data.currentProgress);
                        progress.setMax(data.questionsCount);
                        placeQuestion(data.question);
                    } else {
                        progress.setCurrent(progress.getMax());
                        quizContainer.html(
                            '<h1 class="text-center">Thanks, ' + data.name + '!</h1>'
                            + '<p class="text-center">You responded correctly to ' + data.correct + ' out of ' + data.answers + ' questions!</p>'
                        );
                    }

                    return;
                }

                alert(data.message);
            },
            error: function (e) {
                alert('Something went wrong.');
            }
        });
    });
    //endregion

    var progress = (function () {
        var progressContainer = $('#progress');

        return {
            hide: function () {
                progressContainer.hide();
            },
            show: function (current, max) {
                if (current) {
                    progress.setCurrent(current);
                }

                if (max) {
                    progress.setMax(max);
                }

                progressContainer.show();
            },
            getMax: function () {
                return progressContainer.find('div').attr('aria-valuemax');
            },
            setMax: function (value) {
                progressContainer.find('div').attr('aria-valuemax', value);
            },
            setCurrent: function (value) {
                progressContainer
                    .find('div')
                    .attr('aria-valuenow', value)
                    .css('width', (value / progress.getMax() * 100) + '%')
                ;
            }
        };
    })();
});