<?php

namespace Tests;

use App\Application;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class ApplicationTest extends MockeryTestCase
{
    public function testRegisterShouldReturnTheCorrectObject()
    {
        $application = new Application(Mockery::mock('Request'));
        $application->register('test', null, function () {
            return 123;
        });

        $this->assertSame(123, $application->getService('test'));
        $this->assertNotSame(1234, $application->getService('test'));

        // Match by class name
        $application->register('test2', '\SecondService', function () {
            return 456;
        });

        $this->assertSame(456, $application->getService('\SecondService'));
        $this->assertNotSame(4567, $application->getService('\SecondService'));
    }
}