CREATE TABLE `quizes` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` INT(11) NOT NULL,
  `question` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (quiz_id) REFERENCES quizes(id)
) ENGINE=MyISAM;

CREATE TABLE `answers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_id` INT(11) NOT NULL,
  `answer` VARCHAR(150) NOT NULL,
  `is_correct` BOOLEAN NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FOREIGN KEY (question_id) REFERENCES questions(id)
) ENGINE=MyISAM;

CREATE TABLE `user_answers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` INT(11) NOT NULL,
  `answer_id` INT(11) NOT NULL,
  `user` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_correct` BOOLEAN NOT NULL DEFAULT '0',
  `created_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (quiz_id) REFERENCES quizes(id),
  FOREIGN KEY (answer_id) REFERENCES answers(id)
) ENGINE=MyISAM;