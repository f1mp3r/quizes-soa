#Installation and setup#

1. Prerequisites:
    1. PHP 7.2
    2. MySQL
    3. PHP in command line (for easiest running)
    4. Composer
2. Setup:
    1. Run `composer install`
    2. Set up correct configuration in database.yaml
    3. Execute the sql dump (database.sql)
    4. Insert sample quizes, questions and answers in the database
3. Running:
    1. To spin up the website, run `sudo php run.php` in the root directory
    2. The url of the website is 127.0.0.1:2345
    3. To run the test suite, run `./vendor/bin/phpunit tests/`

---
- Q: Why did I build a mini-framework?
    - A: As it is necessary to use some form of abstraction. I was not sure whether I could use Symfony components / GitHub packages so I wrote many of the essentials from scratch.
- Q: What packages/components did I use?
    - A: psr-4 autoloader, (--dev) phpunit, (--dev) mockery.
- Q: bad practices?
    - Yes, there are some bad practices (not everything is SOLID), unpolished code here and there and not fully functioning parts (Response::view() can't take parameters because I only used it instead of include()-ing the template in the route). I left them like that because a fully-fledged framework with all the tests and everything would have taken me a lot more time.
- Q: Service-oriented?
    - A: I decided to use routes to different services (Auth, Quiz) instead of writing them as separate standalone apps since that would have increased the complexity a lot. The idea would have been the same, just better implemented. The services can be easily broken up now if needed.
