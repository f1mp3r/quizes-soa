<?php

require '../vendor/autoload.php';

$app = new \App\Application(
    new \App\Request(
        $_GET,
        $_POST,
        $_REQUEST,
        $_SERVER,
        $_FILES
    )
);
session_start();

require_once('services.php');
require_once('routes.php');

echo $app->getResponse();