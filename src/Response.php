<?php

namespace App;

use Exception;

class Response
{
    const TEMPLATE_PATH = '../templates/';

    /**
     * @param $html
     * @param int $statusCode
     * @return string
     */
    public static function html($html, int $statusCode = 200): string
    {
        header('Content-Type: text/html');
        http_response_code($statusCode);

        return $html;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return string
     */
    public static function json($data, int $statusCode = 200): string
    {
        header('Content-Type: application/json');
        http_response_code($statusCode);

        return json_encode($data);
    }

    /**
     * @param $view
     * @param int $statusCode
     * @return string
     * @throws Exception
     */
    public static function view($view, int $statusCode = 200)
    {
        if (file_exists(self::TEMPLATE_PATH . $view . '.html')) {
            return self::html(
                file_get_contents(self::TEMPLATE_PATH . $view . '.html'),
                $statusCode
            );
        }

        throw new Exception('View ' . self::TEMPLATE_PATH . $view . '.html' . ' not found');
    }
}