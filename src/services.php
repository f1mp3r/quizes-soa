<?php

use Auth\Authenticator;
use Auth\AuthenticatorInterface;
use Auth\JWTConfiguration;
use Quiz\QuizApi;

$config = yaml_parse_file('../database.yml');

$app->register('authenticator', AuthenticatorInterface::class, function () {
    return new Authenticator();
});

$app->register('quiz', QuizApi::class, function () use ($config) {
    return new QuizApi($config);
});