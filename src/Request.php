<?php

namespace App;

class Request
{
    private $getParameters = [];
    private $postParameters = [];
    private $requestParameters = [];
    private $serverParameters = [];
    private $files = [];

    public function __construct(array $get = [], array $post = [], array $request = [], array $server = [], array $files = [])
    {
        $this->getParameters = $get;
        $this->postParameters = $post;
        $this->requestParameters = $request;
        $this->serverParameters = $server;
        $this->files = $files;
    }

    /**
     * Get parameter from the GET query
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        return $this->getParameters[$key] ?? $default;
    }

    /**
     * Get parameter from the POST query
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function getPostParameter($key, $default = null)
    {
        return $this->postParameters[$key] ?? $default;
    }

    /**
     * Gets parameters from the POST query
     *
     * @param $key
     * @param $default
     * @return mixed
     */
    public function request($key, $default = null)
    {
        return $this->postParameters[$key] ?? $default;
    }

    /**
     * Get parameter from the SERVER global
     *
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function server($key, $default = null)
    {
        return $this->serverParameters[$key] ?? $default;
    }

    /**
     * Get the current URI path
     *
     * @return mixed
     */
    public function getPath()
    {
        return parse_url($this->server('REQUEST_URI'))['path'];
    }

    /**
     * Get the HTTP method
     *
     * @return string
     */
    public function getMethod()
    {
        return strtolower($this->server('REQUEST_METHOD', 'GET'));
    }
}