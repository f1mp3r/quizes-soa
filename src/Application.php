<?php

namespace App;

use ReflectionFunction;

class Application
{
    /**
     * @var Request
     */
    private $request;

    private $services;

    private $serviceClasses;

    private $routes;

    public function __construct($request)
    {
        $this->request = $request;
        $this->services = [];
        $this->serviceClasses = [];
        $this->routes = [];

        $this->register('request', Request::class, function () use ($request) {
            return $request;
        });
    }

    public function register($key, $className, $implementation)
    {
        $this->serviceClasses[$className] = $key;
        $this->services[$key] = call_user_func($implementation, $this);
    }

    public function getService(string $serviceName)
    {
        if (isset($this->services[$serviceName])) {
            return $this->services[$serviceName];
        }

        if (isset($this->serviceClasses[$serviceName])) {
            return $this->services[$this->serviceClasses[$serviceName]];
        }

        throw new \Exception('Service ' . $serviceName . ' not defined');
    }

    public function get(string $url, callable $handler)
    {
        $this->routes['get'][$url] = $handler;
    }

    public function post(string $url, callable $handler)
    {
        $this->routes['post'][$url] = $handler;
    }

    public function getResponse(): string
    {
        if ($response = $this->match($this->request->getMethod(), $this->request->getPath())) {
            return $response;
        }

        return Response::html('Page not found', 404);
    }

    private function match($method, $path): ?string
    {
        if (isset($this->routes[$method][$path])) {
            $arguments = $this->resolveArguments($this->routes[$method][$path]);

            return call_user_func_array($this->routes[$method][$path], $arguments);
        }

        return null;
    }

    private function resolveArguments(callable $handler): array
    {
        $reflection = new ReflectionFunction($handler);

        $parameters = [];

        foreach ($reflection->getParameters() as $parameter) {
            if ($parameter->getClass() && isset($this->serviceClasses[$parameter->getClass()->getName()])) {
                $parameters[] = $this->services[$this->serviceClasses[$parameter->getClass()->getName()]];
            }
        }

        return $parameters;
    }
}