<?php

use App\Request;
use App\Response;

$app->get('/', function () {
    return Response::view('index');
});

$app->get('/initialize', function () use ($app) {
    $userService = $app->getService('authenticator');
    $isAuthenticated = $userService->isAuthenticated();

    return Response::json([
        'step' => $isAuthenticated ? 1 : 0,
        'quiz' => $isAuthenticated ? $userService->getUserData('quiz') : null,
        'question' => $isAuthenticated ? $userService->getUserData('question') : null,
        'currentProgress' => $isAuthenticated ? $userService->getUserData('answered') : null,
        'questionsCount' => $isAuthenticated
            ? count($app->getService('quiz')->getQuestions($userService->getUserData('quiz')))
            : null,
        'questions' => !$isAuthenticated ? $app->getService('quiz')->getQuizList() : []
    ]);
});

$app->post('/authenticate', function (Request $request) use ($app) {
    $name = $request->getPostParameter('name');
    $quizId = (int) $request->getPostParameter('quiz');

    if (!$name) {
        return Response::json(['error' => 'Name parameter is missing']);
    }

    if (!$app->getService('quiz')->getQuizById($quizId)) {
        return Response::json(['code' => 0, 'message' => 'Invalid quiz!']);
    }
    $quizCount = count($app->getService('quiz')->getQuestions($quizId));

    try {
        if ($app->getService('authenticator')->authenticate(['name' => $name, 'quiz' => $quizId])) {
            return Response::json(['code' => 1, 'name' => $name, 'quiz' => $quizId, 'questionsCount' => $quizCount]);
        }
    } catch (Exception $exception) {
        $userData = $app->getService('authenticator')->getUserData();

        return Response::json([
            'code' => 2,
            'message' => $exception->getMessage(),
            'quiz' => $userData['quiz']
        ]);
    }
});

$app->get('/logout', function () use ($app) {
    $app->getService('authenticator')->logout();
    return Response::html('Logged out!');
});

$app->get('/question', function (Request $request) use ($app) {
    $questionId = $request->get('question_id', 0);
    $quizId = $request->get('quiz_id', 0);

    if (!$app->getService('authenticator')->isAuthenticated()) {
        return Response::json(['code' => 0, 'message' => 'You are not logged in!']);
    }

    if (!$quizId && !$questionId) {
        return Response::json(['code' => 0, 'message' => 'Invalid request.']);
    }

    $question = $questionId
        ? $app->getService('quiz')->getQuestionById($questionId)
        : $app->getService('quiz')->getFirstQuizQuestion($quizId)
    ;

    if (!$question) {
        $app->getService('authenticator')->logout();
        return Response::json(['code' => -1, 'message' => 'No such question']);
    }

    $question['answers'] = $app->getService('quiz')->getAnswers($question['id']);
    $app->getService('authenticator')->updateUserParam('quiz', $question['quiz_id']);
    $currentlyAnswered = $app->getService('authenticator')->getUserData('answered');
    $questionsCount = count($app->getService('quiz')->getQuestions($quizId));

    return Response::json([
        'code' => 1,
        'question' => $question,
        'currentProgress' => $currentlyAnswered,
        'questionsCount' => $questionsCount
    ]);
});

$app->post('/answer', function (Request $request) use ($app) {
    $answerId = $request->getPostParameter('answer_id', 0);
    $questionId = $request->getPostParameter('question_id', 0);
    $userService = $app->getService('authenticator');
    $quizService = $app->getService('quiz');

    if (!$userService->isAuthenticated()) {
        return Response::json(['code' => 0, 'message' => 'You need to be authenticated']);
    }

    if (!$questionId || !$answerId) {
        return Response::json(['code' => 0, 'message' => 'Invalid request!']);
    }

    $correctAnswer = $quizService->getCorrectAnswer($questionId);
    $quiz = $quizService->getQuizByQuestion($questionId);

    // Save answer
    $quizService->saveAnswer([
        'quiz_id' => $quiz['id'],
        'answer_id' => $answerId,
        'user' => $userService->getUserData('name'),
        'is_correct' => (int) ($correctAnswer['id'] == $answerId)
    ]);

    // Update user data
    $answered = $userService->getUserData('answered');
    $userService->updateUserParam('answered', $answered + 1);
    $userData = $userService->getUserData();

    if ($correctAnswer['id'] == $answerId) {
        $userService->updateUserParam('correct', $userService->getUserData('correct') + 1);
    }

    if ($nextQuestion = $quizService->getNextQuestion($questionId)) {
        // We have next question, return it
        $userService->updateUserParam('question', $nextQuestion['id']);
        $nextQuestion['answers'] = $quizService->getAnswers($nextQuestion['id']);
        return Response::json([
            'code' => 1,
            'question' => $nextQuestion,
            'currentProgress' => $userData['answered'],
            'questionsCount' => count($quizService->getQuestions($nextQuestion['quiz_id']))
        ]);
    }

    // This was last answer, log user out
    $userService->logout();

    return Response::json([
        'code' => 1,
        'name' => $userData['name'],
        'correct' => $userData['correct'] ?: 0,
        'answers' => $userData['answered']
    ]);
});