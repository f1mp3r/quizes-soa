<?php
/**
 * Created by PhpStorm.
 * User: christopher
 * Date: 2/21/19
 * Time: 12:07 AM
 */

namespace Quiz\Database;


interface DatabaseInterface
{
    public function __construct(array $databaseConfig);
    public function fetchRow(string $query, array $params = []);
    public function fetchAll(string $query, array $params = []): ?array;
    public function insert(string $table, array $data);
}