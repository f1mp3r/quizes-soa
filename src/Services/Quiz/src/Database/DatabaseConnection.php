<?php

namespace Quiz\Database;

use PDO;
use PDOException;

class DatabaseConnection implements DatabaseInterface
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct(array $dbConnection)
    {
        if (
            !isset($dbConnection['host'])
            || !isset($dbConnection['user'])
            || !isset($dbConnection['pass'])
            || !isset($dbConnection['db_name'])
        ) {
            throw new \Exception('Missing parameters. Parameters needed are host, user, pass, db_name');
        }

        $charset = $dbConnection['charset'] ?? 'utf8';

        $dsn = sprintf(
            'mysql:host=%s;dbname=%s;charset=%s',
            $dbConnection['host'],
            $dbConnection['db_name'],
            $charset
        );

        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {
            $this->pdo = new PDO($dsn, $dbConnection['user'], $dbConnection['pass'], $options);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    private function query(string $query, array $params = [])
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($params);

        return $statement;
    }

    public function fetchRow(string $query, array $params = [])
    {
        return $this->query($query, $params)->fetch();
    }

    public function fetchAll(string $query, array $params = []): ?array
    {
        return $this->query($query, $params)->fetchAll();
    }

    public function insert(string $table, array $data)
    {
        $query = 'INSERT INTO %s (%s) VALUES (%s)';
        $columns = array_keys($data);
        $binds = [];

        foreach ($columns as $index => $column) {
            $columns[$index] = '`' . $column . '`';
            $binds[$index] = ':' . $column;
        }

        $query = sprintf(
            $query,
            $table,
            implode(', ', $columns),
            implode(', ', $binds)
        );
        $data = array_combine($binds, $data);

        $statement = $this->pdo->prepare($query);
        return $statement->execute($data);
    }
}