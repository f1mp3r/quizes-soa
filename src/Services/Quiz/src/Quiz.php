<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

class Quiz implements QuizInterface
{
    private $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function getQuizList(): array
    {
        return $this->database->fetchAll('SELECT id, name FROM quizes');
    }

    public function getById(int $quizId)
    {
        return $this->database->fetchRow('SELECT id, `name` FROM quizes WHERE id = :id', [':id' => $quizId]);
    }

    public function getByQuestion(int $questionId)
    {
        return $this->database->fetchRow(
            'SELECT id, `name` FROM quizes WHERE id = (SELECT quiz_id FROM questions WHERE id = :id)',
            [
                ':id' => $questionId
            ]
        );
    }
}