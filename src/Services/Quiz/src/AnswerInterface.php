<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

interface AnswerInterface
{
    public function __construct(DatabaseInterface $database);
    public function getByQuestion(int $questionId): array;
}