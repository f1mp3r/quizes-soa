<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

interface QuizInterface
{
    public function __construct(DatabaseInterface $database);
    public function getQuizList(): array;
    public function getById(int $quizId);
    public function getByQuestion(int $questionId);
}