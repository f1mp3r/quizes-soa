<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

class Question implements QuestionInterface
{
    private $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function getById(int $id)
    {
        return $this->database->fetchRow(
            'SELECT id, question, quiz_id FROM questions WHERE id = :id',
            [
                ':id' => $id
            ]
        );
    }

    public function getFirstByQuiz(int $quizId)
    {
        return $this->database->fetchRow(
            'SELECT id, question, quiz_id FROM questions WHERE quiz_id = :quiz ORDER BY id ASC LIMIT 1',
            [
                ':quiz' => $quizId
            ]
        );
    }

    public function getNext(int $currentQuestionId)
    {
        return $this->database->fetchRow(
            'SELECT qo.id, qo.question, qo.quiz_id FROM questions qo WHERE qo.quiz_id = (SELECT q.quiz_id FROM questions q WHERE q.id = :question_id) AND qo.id > :question_id_repeat ORDER BY qo.id ASC LIMIT 1',
            [
                ':question_id' => $currentQuestionId,
                ':question_id_repeat' => $currentQuestionId
            ]
        );
    }

    public function getByQuiz(int $quizId)
    {
        return $this->database->fetchRow(
            'SELECT id, question, quiz_id FROM questions WHERE quiz_id = :id',
            [
                ':id' => $quizId
            ]
        );
    }
}