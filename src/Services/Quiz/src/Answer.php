<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

class Answer implements AnswerInterface
{
    private $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function getByQuestion(int $questionId): array
    {
        return $this->database->fetchAll(
            'SELECT id, answer FROM answers WHERE question_id = :question',
            [
                ':question' => $questionId
            ]
        );
    }

    public function getCorrectAnswerForQuestion($questionId)
    {
        return $this->database->fetchRow(
            'SELECT id, answer FROM answers WHERE is_correct = 1 AND question_id = :question_id',
            [
                ':question_id' => $questionId
            ]
        );
    }
}