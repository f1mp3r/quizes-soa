<?php

namespace Quiz;

use Quiz\Database\DatabaseConnection;

class QuizApi
{
    private $quizes;
    private $answers;
    private $questions;
    private $database;
    private $logsTable = 'user_answers';

    public function __construct(array $dbConfig)
    {
        $database = new DatabaseConnection($dbConfig);

        $this->quizes = new Quiz($database);
        $this->answers = new Answer($database);
        $this->questions = new Question($database);
        $this->database = $database;
    }

    public function getQuizList()
    {
        return $this->quizes->getQuizList();
    }

    public function getQuestions(int $quizId)
    {
        return $this->questions->getByQuiz($quizId);
    }

    public function getQuestionById(int $questionId)
    {
        return $this->questions->getById($questionId);
    }

    public function getFirstQuizQuestion(int $quizId)
    {
        return $this->questions->getFirstByQuiz($quizId);
    }

    public function getQuizById(int $quizId)
    {
        return $this->quizes->getById($quizId);
    }

    public function getAnswers(int $questionId): array
    {
        return $this->answers->getByQuestion($questionId);
    }

    public function getCorrectAnswer(int $questionId)
    {
        return $this->answers->getCorrectAnswerForQuestion($questionId);
    }

    public function getQuizByQuestion(int $questionId)
    {
        return $this->quizes->getByQuestion($questionId);
    }

    public function getNextQuestion(int $currentQuestionId)
    {
        return $this->questions->getNext($currentQuestionId);
    }

    public function saveAnswer(array $answerData)
    {
        if (
            !isset($answerData['quiz_id'])
            || !isset($answerData['answer_id'])
            || !isset($answerData['user'])
            || !isset($answerData['is_correct'])
        ) {
            throw new \Exception('One or more columns are missing (required: quiz_id, answer_id, user, is_correct)');
        }

        if (!isset($answerData['created_at'])) {
            $answerData['created_at'] = (new \DateTime())->format('Y-m-d H:i:s');
        }

        return $this->database->insert($this->logsTable, $answerData);
    }
}