<?php

namespace Quiz;

use Quiz\Database\DatabaseInterface;

interface QuestionInterface
{
    public function __construct(DatabaseInterface $database);
    public function getById(int $id);
    public function getByQuiz(int $quizId);
    public function getFirstByQuiz(int $quizId);
    public function getNext(int $currentQuestionId);
}