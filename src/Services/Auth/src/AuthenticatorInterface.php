<?php

namespace Auth;

interface AuthenticatorInterface
{
    public function isAuthenticated(): bool;

    public function authenticate(array $userData): AuthenticatorInterface;

    public function getUserData(string $key = null);

    public function updateUserParam(string $key, $value): AuthenticatorInterface;

    public function logout(): AuthenticatorInterface;
}