<?php

namespace Auth;

class Authenticator implements AuthenticatorInterface
{
    const SESSION_KEY = 'au_userdata';

    public function isAuthenticated(): bool
    {
        return isset($_SESSION[self::SESSION_KEY]);
    }

    public function authenticate(array $userData): AuthenticatorInterface
    {
        if ($this->isAuthenticated()) {
            throw new \Exception('You are already authenticated');
        }

        $_SESSION[self::SESSION_KEY] = json_encode($userData);

        return $this;
    }

    public function getUserData(string $key = null)
    {
        if (!$this->isAuthenticated()) {
            return [];
        }

        $userData = json_decode($_SESSION[self::SESSION_KEY], true);

        return $key ? ($userData[$key] ?? null) : $userData;
    }

    public function updateUserParam(string $key, $value): AuthenticatorInterface
    {
        if (!$this->isAuthenticated()) {
            throw new \Exception('User not authenticated');
        }

        $userData = $this->getUserData();
        $userData[$key] = $value;

        $_SESSION[self::SESSION_KEY] = json_encode($userData);

        return $this;
    }

    public function logout(): AuthenticatorInterface
    {
        unset($_SESSION[self::SESSION_KEY]);

        return $this;
    }
}